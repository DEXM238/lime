google.maps.event.addDomListener(window, 'load', initialize);
var map;
function initialize() {
map = new google.maps.Map(document.getElementById('map'), {
		  zoom: 13,
		  center: new google.maps.LatLng(37.789879, -122.390442),
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		
		google.maps.event.addListener(map, 'click', function(event) {
			$('input[name="Marker[latitude]"]').attr("value",event.latLng.lat());
			$('input[name="Marker[longitude]"]').attr("value",event.latLng.lng());
		});
}