<?php

class ApiController extends Controller
{
    public function filters()
    {
        return array(
           // 'postOnly + createUser',
        );
    }
		
	private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
	{
		// set the status
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		header($status_header);
		// and the content type
		header('Content-type: ' . $content_type);
	 
		// pages with body are easy
		if($body != '')
		{
			// send the body
			echo $body;
		}
		// we need to create the body if none is passed
		else
		{
			// create some body messages
			$message = '';
	 
			// this is purely optional, but makes the pages a little nicer to read
			// for your users.  Since you won't likely send a lot of different status codes,
			// this also shouldn't be too ponderous to maintain
			switch($status)
			{
				case 401:
					$message = 'You must be authorized to view this page.';
					break;
				case 404:
					$message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
					break;
				case 500:
					$message = 'The server encountered an error processing your request.';
					break;
				case 501:
					$message = 'The requested method is not implemented.';
					break;
			}
	 
			// servers don't always have a signature turned on 
			// (this is an apache directive "ServerSignature On")
			$signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];
	 
			// this should be templated in a real-world solution
			$body = '
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
	</head>
	<body>
		<h1>' . $this->_getStatusCodeMessage($status) . '</h1>
		<p>' . $message . '</p>
		<hr />
		<address>' . $signature . '</address>
	</body>
	</html>';
	 
			echo $body;
		}
		Yii::app()->end();
	}
	private function _getStatusCodeMessage($status)
	{
		// these could be stored in a .ini file and loaded
		// via parse_ini_file()... however, this will suffice
		// for an example
		$codes = Array(
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		);
		return (isset($codes[$status])) ? $codes[$status] : '';
	}
	/**
	 * This is test page
	 */
	public function actionMarker($marker_id=null, $date=0)
	{
				$modification_date = Yii::app()->getRequest()->getQuery('date');
		if(isset($marker_id))
		{
			$marker = Marker::model()->findByPk($marker_id);
                        header('Last-Modified: ' . 'Wed, 17 Mar 2010 10:13:25 GMT');
			echo json_encode($marker->getAttributes(array('name', 'latitude', 'longitude', 'category_id')));
		}
		else
		{
			if(isset($modification_date))
			{
				$marker = Marker::model()->findAllByAttributes(array(), 'modification_date > :date', array(':date' => $modification_date));
			}
			else
			{
				$marker = Marker::model()->findAll();
			}
			$data=array_map(create_function('$m','return $m->getAttributes(array(\'marker_id\',\'name\',\'latitude\',\'longitude\', \'category_id\' ));'),$marker);
			header('Last-Modified: ' . 'Wed, 17 Mar 2010 10:13:25 GMT');
                        echo json_encode($data);
		}
	}
		
	public function actionCreateUser()
	{
            $model = new User;
            
            foreach($_POST as $var=>$value) {
                $model->$var = $value;
            }
            $model->service_id = '1';
            if($model->save()) 
            {
                // set the status
		$status_header = 'HTTP/1.1 ' . 201 . ' ' . 'Created';
		header($status_header);
		// and the content type
		header('Location: ' . 'lime.loc/user/'.$model->user_id);
            }
            else
                // set the status
		$status_header = 'HTTP/1.1 ' . 400 . ' ' . 'Bad Request';
                header($status_header);
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}