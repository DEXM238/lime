<?php
/* @var $this MarkerController */
/* @var $data Marker */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('marker_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->marker_id), array('view', 'id'=>$data->marker_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('latitude')); ?>:</b>
	<?php echo CHtml::encode($data->latitude); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('longitude')); ?>:</b>
	<?php echo CHtml::encode($data->longitude); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />
</div>