<?php
/* @var $this MarkerController */
/* @var $model Marker */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'marker-form',
        'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля із <span class="required">*</span> обов'язкові.</p>

	<?php echo $form->errorSummary($model); ?>
        
        <?php echo $form->map(); ?>

        <?php echo $form->textFieldRow($model,'latitude'); ?>
        
        <?php echo $form->textFieldRow($model,'longitude'); ?>
        
        <?php echo $form->textAreaRow($model,'name', array('class'=>'span4', 'rows'=>5)); ?>
        
        <?php echo $form->dropDownListRow($model,'category_id', $model->getCategories()); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Додати',
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->