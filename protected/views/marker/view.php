<?php
/* @var $this MarkerController */
/* @var $model Marker */

$this->breadcrumbs=array(
	'Markers'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Список маркерів', 'url'=>array('index')),
	array('label'=>'Додати маркер', 'url'=>array('create')),
	array('label'=>'Змінити маркер', 'url'=>array('update', 'id'=>$model->marker_id)),
	array('label'=>'Видалити марке', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->marker_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Керування маркерами', 'url'=>array('admin')),
);
?>

<h1>Маркер №<?php echo $model->marker_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
        'type'=>array('striped', 'bordered'),
	'data'=>$model,
	'attributes'=>array(
		'marker_id',
		'latitude',
		'longitude',
		'name',
                array(
                    'label' => 'Категорія',
                    'value' => $model->category->title,
                ),
	),
)); ?>