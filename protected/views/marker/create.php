<?php
/* @var $this MarkerController */
/* @var $model Marker */
Yii::app()->getClientScript()->registerScriptFile( Yii::app()->assetManager->baseUrl.'/my.js' );
$this->breadcrumbs=array(
	'Маркери'=>array('index'),
	'Додати',
);

$this->menu=array(
	array('label'=>'Список маркерів', 'url'=>array('index')),
	array('label'=>'Редагування', 'url'=>array('admin')),
);
?>

<h1>Додати маркер</h1>  


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
