<?php
/* @var $this MarkerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Маркери',
);

$this->menu=array(
	array('label'=>'Додати маркер', 'url'=>array('create')),
	array('label'=>'Керування маркерами', 'url'=>array('admin')),
);
?>

<h1>Маркери</h1>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
