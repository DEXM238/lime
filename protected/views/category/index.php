<?php
/* @var $this CategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Категорії',
);

$this->menu=array(
	array('label'=>'Додати категорію', 'url'=>array('create')),
	array('label'=>'Керування категоріями', 'url'=>array('admin')),
);
?>

<h1>Категорії</h1>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
