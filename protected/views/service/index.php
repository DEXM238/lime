<?php
/* @var $this ServiceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Сервіси',
);

$this->menu=array(
	array('label'=>'Додати сервіс', 'url'=>array('create')),
	array('label'=>'Керування сервісами', 'url'=>array('admin')),
);
?>

<h1>Сервіси</h1>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
